﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Http.HttpResults;
using Microsoft.AspNetCore.Mvc;
using System.Net;
using UserApi.Interfaces;
using UserApi.Models;


namespace UserApi.Controllers
{
    [Route("api/user")]
    [ApiController]
    public class UserController : ControllerBase
    {
        private IUser _user;
        public UserController(IUser user) {
            _user = user;
        }

        [HttpGet]
        public IActionResult GetUser()
        {
            var data = _user.GetUser();
            return new JsonResult(new { status = 200, data = data });
        }


        [HttpPost]
        public IActionResult AddUser(UserModel user)
        {
            var passwordEnc = new Helpers.GlobalFunction().GetMD5(user.password);
            var result = _user.AddUser(user, passwordEnc);
            if(result == -1)
            {
                return new JsonResult(new { status = 400, data = "Username exist" });
            }
            else if(result > 0)
            {
                return new JsonResult(new { status = 200, data = "Success" });
            }
            else
            {
                return new JsonResult(new { status = 500, data = "Failed" });
            }
        }


        [HttpPut("{id}")]
        public IActionResult UpdateUser(int id, UpdateUserModel user)
        {
            var result = _user.UpdateUser(user, id);
            return new JsonResult(new { status = 200, data = "Updated" });
        }

        [HttpDelete("{id}")]
        public IActionResult DeleteUser(int id)
        {
            var result = _user.DeleteUser(id);
            return new JsonResult(new { status = 200, data = "Deleted" });
        }


    }
}
