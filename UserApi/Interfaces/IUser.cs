﻿using UserApi.Models;

namespace UserApi.Interfaces
{
    public interface IUser
    {
        List<ListUserModel> GetUser();
        int AddUser(UserModel model, string passEnc);
        bool UpdateUser(UpdateUserModel model, int user_id);
        bool DeleteUser(int user_id);
    }
}
