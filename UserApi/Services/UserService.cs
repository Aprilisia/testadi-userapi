﻿using Microsoft.Data.SqlClient;
using Microsoft.Extensions.Options;
using System.Text.Json;
using UserApi.Helpers;
using UserApi.Interfaces;
using UserApi.Models;
using static Microsoft.EntityFrameworkCore.DbLoggerCategory.Database;

namespace UserApi.Services
{
    public class UserService : IUser
    {
        private readonly Helpers.AppSettings _appSettings;
        public UserService(IOptions<AppSettings> appSettings)
        {
            _appSettings = appSettings.Value;
        }

        public List<ListUserModel> GetUser()
        {
            List<ListUserModel> result = new List<ListUserModel>();
            var sql = "";
            try
            {
                using (var cs = new ConnectionService(_appSettings).GetConnection())
                {
                    sql = "SELECT " +
                            "A.user_id, A.role_id, B.name as role_name, " +
                            "A.name, A.address, A.phone_number, A.username, A.password, A.created_at, A.updated_at " +
                            "FROM tbl_user A " +
                            "JOIN tbl_role B ON A.role_id = B.role_id ";

                    SqlCommand command = new SqlCommand(sql, cs);
                    cs.Open();
                    var reader = command.ExecuteReader();
                    if (reader.HasRows)
                    {
                        while (reader.Read())
                        {
                            result.Add(new ListUserModel()
                            {
                                user_id = Convert.ToInt32(reader["user_id"].ToString()),
                                role_id = Convert.ToInt32(reader["role_id"].ToString()),
                                role_name = reader["role_name"] == DBNull.Value ? null : reader["role_name"].ToString(),
                                name = reader["name"] == DBNull.Value ? null : reader["name"].ToString(),
                                address = reader["address"] == DBNull.Value ? null : reader["address"].ToString(),
                                phone_number = reader["phone_number"] == DBNull.Value ? null : reader["phone_number"].ToString(),
                                username = reader["username"] == DBNull.Value ? null : reader["username"].ToString(),
                                password = reader["password"] == DBNull.Value ? null : reader["password"].ToString(),
                                created_at = Convert.ToDateTime(reader["created_at"].ToString()),
                                updated_at = Convert.ToDateTime(reader["updated_at"].ToString())
                            });
                        }
                    }
                    cs.Close();
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            return result;
        }

        public int AddUser(UserModel model, string passEnc)
        {
            int returnValue = -1;
            var sql = "";
            try
            {
                using (SqlConnection cs = new SqlConnection(_appSettings.ConnectionString))
                {
                    sql = "INSERT INTO tbl_user (" +
                            "role_id, name, phone_number, address, username, password)" +
                            "VALUES(@role_id, @name, @phone_number, @address, @username, @password)";
                    SqlCommand command = new SqlCommand(sql, cs);
                    command.Parameters.AddWithValue("@role_id", model.role_id);
                    command.Parameters.AddWithValue("@name", model.name);
                    command.Parameters.AddWithValue("@phone_number", model.phone_number);
                    command.Parameters.AddWithValue("@address", model.address);
                    command.Parameters.AddWithValue("@username", model.username);
                    command.Parameters.AddWithValue("@password", passEnc);

                    cs.Open();

                    returnValue = command.ExecuteNonQuery() == 1 ? 1 : -1;
                    cs.Close();
                }

            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            return returnValue;
        }

        public bool UpdateUser(UpdateUserModel model, int user_id)
        {
            var result = false;
            var sql = "";
            try
            {
                using (SqlConnection cs = new SqlConnection(_appSettings.ConnectionString))
                {
                    cs.Open();
                    sql = "UPDATE tbl_user SET role_id=@role_id, " +
                            "name=@name, phone_number=@phone_number, address=@address, updated_at = @updated_at " +
                            "WHERE user_id =@user_id";

                    SqlCommand command = new SqlCommand(sql, cs);
                    command.Parameters.AddWithValue("@role_id", model.role_id);
                    command.Parameters.AddWithValue("@name", model.name);
                    command.Parameters.AddWithValue("@phone_number", model.phone_number);
                    command.Parameters.AddWithValue("@address", model.address);
                    command.Parameters.AddWithValue("@updated_at", DateTime.Now);
                    command.Parameters.AddWithValue("@user_id", user_id);

                    result = command.ExecuteNonQuery() == 1;

                    cs.Close();
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            return result;
        }


        public bool DeleteUser(int user_id)
        {
            var result = false;
            var sql = "";
            try
            {
                using (SqlConnection cs = new SqlConnection(_appSettings.ConnectionString))
                {
                    sql = "DELETE FROM tbl_user WHERE user_id = @user_id";
                    SqlCommand command = new SqlCommand(sql, cs);
                    command.Parameters.AddWithValue("@user_id", user_id);
                    cs.Open();

                    result = command.ExecuteNonQuery() == 1;
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            return result;
        }


    }
}
