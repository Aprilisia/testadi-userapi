﻿using Microsoft.Data.SqlClient;
using System.Data.Common;
using System.Data;
using System;

namespace UserApi.Services
{
    public class ConnectionService
    {
        private readonly Helpers.AppSettings _appSettings;
        public ConnectionService() { }
        public ConnectionService(Helpers.AppSettings appSettings)
        {
            _appSettings = appSettings;
        }

        public SqlConnection GetConnection()
        {
            return new SqlConnection(_appSettings.ConnectionString);
        }

        public DbCommand GetCommand()
        {
            return new SqlCommand();
        }

        public SqlParameter GetParameter(string parameter, object value)
        {
            SqlParameter parameterObject = new SqlParameter(parameter, value != null ? value : DBNull.Value);
            parameterObject.Direction = ParameterDirection.Input;
            return parameterObject;
        }

    }
}
