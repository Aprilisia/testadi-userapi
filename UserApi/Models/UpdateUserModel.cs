﻿namespace UserApi.Models
{
    public class UpdateUserModel
    {
        public int user_id { get; set; }
        public int role_id { get; set; }
        public string name { get; set; }
        public string address { get; set; }
        public string phone_number { get; set; }
    }
}
